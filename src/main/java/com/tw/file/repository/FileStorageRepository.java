package com.tw.file.repository;

import com.tw.file.entity.FileData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface FileStorageRepository extends JpaRepository<FileData, Long> {

    Optional<FileData> findByName(String fileName);
}
