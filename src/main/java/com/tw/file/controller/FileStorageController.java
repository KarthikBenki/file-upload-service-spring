package com.tw.file.controller;

import com.tw.file.dto.FileDataDTO;
import com.tw.file.entity.FileData;
import com.tw.file.service.FileStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/file")
@CrossOrigin("*")
public class FileStorageController {

    @Autowired
    private FileStorageService service;

    @GetMapping
    public String checkApi(){
        return "Hello World";
    }


    @GetMapping("/all")
    public ResponseEntity<List<FileDataDTO>> getAllFiles(){
        return ResponseEntity.ok(service.getAllFiles());
    }

    @PostMapping
    public ResponseEntity<?> uploadFile(@RequestParam("file") MultipartFile file) throws IOException {
        String uploadFile = service.uploadFile(file);
        return ResponseEntity.status(HttpStatus.OK)
                .body(uploadFile);
    }

    @GetMapping("/{fileName}")
    public ResponseEntity<?> downloadFile(@PathVariable String fileName) {
        FileData fileData = service.downloadFile(fileName);
        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.parseMediaType(fileData.getType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename = " + fileData.getName())
                .body(new ByteArrayResource((fileData.getFileData())));

    }

    @DeleteMapping("/{fileName}")
    public void deleteFile(@PathVariable String fileName){
        service.deleteFile(fileName);
    }
}
